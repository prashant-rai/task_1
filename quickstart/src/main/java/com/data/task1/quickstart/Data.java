package com.data.task1.quickstart;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class Data {
	
	final static String cs = "jdbc:mysql://localhost:3306/taskdb?useSSL=false";
	final static String user = "root";
	final static String password = "root";

	final static String sql = "INSERT INTO data_archive(serial_number , unix_ts, datapoint ) VALUES(?, ?, ?)";

	private void timer(int loop) {
		
		try (Connection con = DriverManager.getConnection(cs, user, password);
                PreparedStatement pst = con.prepareStatement(sql)) {
        	
        	long start = System.currentTimeMillis();
            for (int i = 1; i <= loop; i++) {

                pst.setInt(1, i * 2);
                pst.setInt(2, i * 2);
                pst.setLong(3, i * 2);
                pst.executeUpdate();
            }
            
            long endTime = System.currentTimeMillis();
            long timeTaken = endTime - start;
            String message = String.format("time taken is %d", timeTaken);
            System.out.println(message);
            
        } catch (SQLException ex) {
        	ex.printStackTrace();
        }
		
	}
	
	private void timer2(int loop) {
		
		try (Connection con = DriverManager.getConnection(cs, user, password);
                PreparedStatement pst = con.prepareStatement(sql)) {
        	
			con.setAutoCommit(false);
        	long start = System.currentTimeMillis();
            for (int i = 1; i <= loop; i++) {

                pst.setInt(1, i * 2);
                pst.setInt(2, i * 2);
                pst.setLong(3, i * 2);
                pst.executeUpdate();
                
               if(i%2000 == 0){      
            	   con.commit();
               }
               
            }
            
            long endTime = System.currentTimeMillis();
            long timeTaken = endTime - start;
            String message = String.format("time taken is %d", timeTaken);
            System.out.println(message);

            
        } catch (SQLException ex) {
        	ex.printStackTrace();
        }
		
	}
	
    public static void main(String[] args) {

        Data dataObj = new Data();
        dataObj.timer2(1000000);
        
        
    }
    
}
